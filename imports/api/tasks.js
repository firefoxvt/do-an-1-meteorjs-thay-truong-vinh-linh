/**
 * Created by phamngochai on 3/15/17.
 */
import { Mongo } from 'meteor/mongo';

export const Tasks = new Mongo.Collection('tasks');
